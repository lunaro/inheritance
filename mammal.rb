require_relative 'animal'

class Mammal < Animal
  def warm_blooded?
    true
  end
  def can_fly?
    false
  end
end