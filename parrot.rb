require_relative 'animal'

class Parrot < Animal
  include Flight

  def can_fly?
    true
  end
  
  def colourful?
    true
  end
end
