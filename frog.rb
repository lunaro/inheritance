require_relative 'amphibian'

class Frog < Amphibian
  attr_reader :num_legs
  def initialize
    super
    @num_legs = 4
  end
end