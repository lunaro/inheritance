module Flight
  attr_accessor :airspeed_velocity

  def initialize
    @airspeed_velocity = 0
  end

  def fly
    puts "I'm a #{self.class.name}, I am flying!"
  end
end