require_relative 'primate'

class Chimpanzee < Primate
  def opposable_thumbs?
    true
  end
end