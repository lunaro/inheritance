require_relative 'frog'
require_relative 'flight'
require_relative 'bat'
require_relative 'chimpanzee'
require_relative 'parrot'

describe Frog do
  it 'should have 4 legs' do
    frog = Frog.new
    expect(frog.num_legs).to be 4
  end
  it 'should be alive' do
    frog = Frog.new
    expect(frog.alive).to be true
  end
  it 'should be cold blooded' do
    frog = Frog.new
    expect(frog.warm_blooded?).to be false
  end
end

describe Parrot do
  it 'should fly' do
    parrot = Parrot.new
    expect(parrot.can_fly?).to be true
  end
end

describe Bat do
  it 'should be able to fly' do
    bat = Bat.new
    expect(bat.can_fly?).to be true
  end
  it 'should fly' do
    bat = Bat.new
    expect(bat.fly).to eql("I am a bat, I'm flying!")
  end
end

describe Chimpanzee do
  it 'should have thumbs' do
    chimp = Chimpanzee.new
    expect(chimp.opposable_thumbs?).to be true
  end
end