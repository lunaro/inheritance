require_relative 'mammal'
require_relative 'flight'

class Bat < Mammal
  include Flight
  
  def can_fly?
    true
  end

  def fly
    "I am a bat, I'm flying!"
  end
end