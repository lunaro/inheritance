require_relative 'mammal'

class Primate < Mammal
  attr_reader :num_legs

  def initialize
    super
    @num_legs = 2
  end
end