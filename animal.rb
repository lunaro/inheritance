class Animal
  attr_reader :alive, :motile, :heterotroph

  def initialize
    @alive = true
    @motile = true
    @heterotroph = true
  end
end